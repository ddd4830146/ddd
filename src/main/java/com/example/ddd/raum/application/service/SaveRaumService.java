package com.example.ddd.raum.application.service;

import com.example.ddd.raum.domain.model.Raum;
import com.example.ddd.raum.domain.repository.RaumRepository;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class SaveRaumService {
    private RaumRepository raumRepository;

    private boolean isUniqueRaumNummer(UUID id){
        return raumRepository.geById(id).isEmpty();
    }

    public void save(Raum raum){
        if (!isUniqueRaumNummer(raum.getUuid())) {
             throw new IllegalArgumentException("Ungültige Paramiters");
        }

        raumRepository.save(raum);
    }

}
