package com.example.ddd.raum.infrastructure.dto;

public record RaumRequest(String raumName, String raumNummer) {

}
