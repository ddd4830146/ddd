package com.example.ddd.raum.infrastructure.valueObject;

import jakarta.persistence.Embeddable;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

@Embeddable
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class RaumNummer {
    String raumNummer;
}
