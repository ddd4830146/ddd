package com.example.ddd.raum.infrastructure.valueObject;


import jakarta.persistence.Embeddable;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;


@Embeddable
@FieldDefaults(level = AccessLevel.PRIVATE)
@NoArgsConstructor
public class RaumName {
    String raumName;

}
