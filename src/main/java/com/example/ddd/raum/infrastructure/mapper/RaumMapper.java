package com.example.ddd.raum.infrastructure.mapper;

import com.example.ddd.raum.domain.model.Raum;
import com.example.ddd.raum.domain.model.valueObject.RaumName;
import com.example.ddd.raum.domain.model.valueObject.RaumNummer;
import com.example.ddd.raum.infrastructure.dto.RaumRequest;
import lombok.NonNull;
import org.springframework.stereotype.Component;

@Component
public class RaumMapper {
    public Raum toDomain(@NonNull RaumRequest raumRequest){
        return new Raum(
                null,
                new RaumName(raumRequest.raumName()),
                new RaumNummer(raumRequest.raumNummer())
        );
    }
}
