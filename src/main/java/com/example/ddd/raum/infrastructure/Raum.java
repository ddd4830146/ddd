package com.example.ddd.raum.infrastructure;

import com.example.ddd.raum.infrastructure.valueObject.RaumName;
import com.example.ddd.raum.infrastructure.valueObject.RaumNummer;
import jakarta.persistence.Column;
import jakarta.persistence.Embedded;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

import java.util.UUID;

@Entity
@FieldDefaults(level = AccessLevel.PRIVATE)
@NoArgsConstructor
public class Raum {
    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    UUID uuid;
    @Embedded
    @Column
    RaumName raumName;

    @Embedded
    @Column
    RaumNummer raumNummer;
}
