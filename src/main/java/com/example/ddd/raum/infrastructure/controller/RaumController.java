package com.example.ddd.raum.infrastructure.controller;

import com.example.ddd.raum.application.service.SaveRaumService;
import com.example.ddd.raum.infrastructure.dto.RaumRequest;
import com.example.ddd.raum.infrastructure.mapper.RaumMapper;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/room")
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class RaumController {
    SaveRaumService saveRaumService;
    RaumMapper      raumMapper;
    @PostMapping
    public ResponseEntity<?> save(RaumRequest raumRequest){
        saveRaumService.save(raumMapper.toDomain(raumRequest));
        return ResponseEntity.ok().body("saved");
    }
}
