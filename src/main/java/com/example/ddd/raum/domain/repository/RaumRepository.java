package com.example.ddd.raum.domain.repository;

import com.example.ddd.raum.domain.model.Raum;

import java.util.Optional;
import java.util.UUID;

public interface RaumRepository{
    Optional<Raum> geById(UUID id);
    void save(Raum raum);
}
