package com.example.ddd.raum.domain.model;

import com.example.ddd.raum.domain.model.valueObject.RaumName;
import com.example.ddd.raum.domain.model.valueObject.RaumNummer;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;

import java.util.UUID;


@Getter
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@RequiredArgsConstructor
public class Raum {
    UUID       uuid;
    RaumName   raumName;
    RaumNummer raumNummer;

}
