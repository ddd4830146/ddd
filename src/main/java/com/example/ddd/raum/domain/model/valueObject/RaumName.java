package com.example.ddd.raum.domain.model.valueObject;


import jakarta.persistence.Embeddable;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.experimental.FieldDefaults;

@Getter
@FieldDefaults(level = AccessLevel.PRIVATE)
public class RaumName {
    String raumName;

    public RaumName(@NonNull String raumName) {
        this.raumName = raumName;
    }
}
