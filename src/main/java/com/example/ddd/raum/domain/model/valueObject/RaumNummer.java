package com.example.ddd.raum.domain.model.valueObject;

import com.example.ddd.raum.domain.model.Raum;
import jakarta.persistence.Embeddable;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.experimental.FieldDefaults;
@Getter
@FieldDefaults(level = AccessLevel.PRIVATE)
public class RaumNummer {
    String raumNummer;

    public RaumNummer(@NonNull String raumNummer) {
        if(raumNummer.length() == 4){
            throw new IllegalArgumentException("More than 4 Nummers");
        }
        this.raumNummer = raumNummer;
    }
}
