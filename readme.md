# Projektbeschreibung

Dieses Projekt ist ein einfaches Beispiel für eine Spring Boot-Anwendung, die eine RESTful API bereitstellt. Die API antwortet auf Anfragen unter dem Pfad "/api/hello" und gibt eine einfache "Hello World"-Nachricht zurück.

## Voraussetzungen

Um dieses Projekt auszuführen, müssen Sie die folgenden Voraussetzungen erfüllen:

- Java Development Kit (JDK) 17 oder höher
- Apache Maven
- Eine Entwicklungsumgebung Ihrer Wahl (z. B. IntelliJ IDEA oder Eclipse)

## Ausführung des Projekts

1. Klone das Projekt von Gitlab oder lade es herunter.

2. Navigieren Sie zum Projektverzeichnis.

3. Öffnen Sie ein Terminal oder eine Befehlszeile in diesem Verzeichnis.

4. Führen Sie den folgenden Maven-Befehl aus, um das Projekt zu kompilieren und auszuführen:

   ```bash
   mvn spring-boot:run
Nach erfolgreicher Ausführung können Sie die API unter http://localhost:8080/api/hello aufrufen und sollten die "Hello World"-Nachricht sehen.

## Verwendung

Sie können diese API verwenden, um einen einfachen Gesundheitscheck durchzuführen, indem Sie http://localhost:8080/api/hello aufrufen. Sie erhalten eine Antwort mit der Nachricht "Hello World".

## Abhängigkeiten

Dieses Projekt verwendet die folgenden Hauptabhängigkeiten:

* Spring Boot: Eine leistungsstarke Framework für die Entwicklung von Java-Anwendungen.
* Project Lombok: Ein hilfreiches Werkzeug zur Vereinfachung des Java-Quellcodes durch Annotationen.
* Spring Boot Starter Web: Eine Abhängigkeit, die die Entwicklung von Webanwendungen mit Spring Boot erleichtert.

## Lizenz

Dieses Projekt ist unter der MIT-Lizenz lizenziert.

Denk daran, die Informationen im README an deine spezifischen Projektanforderungen anzupassen. Dieses README bietet eine allgemeine Vorlage, die du verwenden kannst, um die Benutzer deines Projekts über die Installation, Ausführung und Verwendung zu informieren.
